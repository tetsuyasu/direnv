#!/bin/bash

set -e

if type brew >/dev/null 2>&1; then
    brew install git-filter-repo
else
    echo not supported, exit.
    exit 1
fi

if [ -f .git/config ]; then
    url=$(grep 'url = ' .git/config | awk -F= '{print $2}' | sed -e 's|/$||')
else
    echo .git/config not fount, exit.
    exit 1
fi

repo="$(basename "$url")"
echo "url: $url, repo: $repo"

mkdir -p "/tmp/$$"
cd "/tmp/$$"
rm -rf "$repo"
git clone --bare "$url"
cd "/tmp/$$/$repo"
git filter-repo --strip-blobs-bigger-than 10M
git filter-repo --blob-callback '
  if blob.data.startswith(b"version https://git-lfs.github.com/spec/v1"):
    size_in_bytes = int.from_bytes(blob.data[124:], byteorder="big")
    if size_in_bytes > 10*1000:
      blob.skip()
  '
git push origin --force --all
git push origin --force --tags

rm -rf "/tmp/$$"

echo "OK $0"

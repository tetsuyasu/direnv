#!/bin/bash

set -e

if [ "$*" != "" ]; then
    repos="$*"
else
    repos="$(ls -d ./*)"
fi

for i in $repos; do
    if [ ! -d "$i" ]; then
	continue
    fi
    if [ ! -f "$i/setup.sh" ]; then
	continue
    fi
    pushd .
    cd "$i"
    git pull
    echo "$i"
    ../direnv/clean.sh
    echo "$i"
    if [ -f pyenv.sh ]; then
	./pyenv.sh
    elif [ -f venv.sh ]; then
	./venv.sh
    else
	../direnv/venv.sh
    fi
    echo "$i"
    if [ -f node.sh ]; then
	./node.sh
    fi
    echo "$i"
    direnv exec . ./setup.sh
    popd
done

echo "OK $0"

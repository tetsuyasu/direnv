#!/bin/bash

set -e

DIR="$(cd $(dirname "$0"); pwd)"

# env check
if [ "$PYENV_VIRTUAL_ENV" != "" ]; then
    echo "$PYENV_VIRTUAL_ENV"
elif [ "$VIRTUAL_ENV" != "" ]; then
    echo "$VIRTUAL_ENV"
elif [ -f /.dockerenv ]; then
    echo NVIDIA_BUILD_ID="$NVIDIA_BUILD_ID"
else
    echo please activate python3 environment.
    exit 1
fi

function install_grpcio() {
    export GRPC_PYTHON_BUILD_SYSTEM_OPENSSL=1
    export GRPC_PYTHON_BUILD_SYSTEM_ZLIB=1
    #pip install "grpcio<1.40"
    pip install grpcio
}

function install_llvmlite() {
    brew install llvm@11
    LLVM_CONFIG="$(brew --prefix llvm@11)/bin/llvm-config" pip install llvmlite
}

function install_bazel() {
    # Step 1: Install Xcode command line tools
    xcode-select --install || echo OK
    #sudo xcodebuild -license accept

    # Step 2: Download the Bazel installer
    export BAZEL_VERSION=3.7.2
    curl -fLO "https://github.com/bazelbuild/bazel/releases/download/${BAZEL_VERSION}/bazel-${BAZEL_VERSION}-installer-darwin-x86_64.sh"

    # Step 3: Run the installer
    chmod +x "bazel-${BAZEL_VERSION}-installer-darwin-x86_64.sh"
    ./bazel-${BAZEL_VERSION}-installer-darwin-x86_64.sh --user
}

pip install -U pip

case "$1" in
    grpcio)
	install_grpcio
	;;
    opencv-python)
	if [ ! -d opencv-python ]; then
	    git clone --recursive https://github.com/skvark/opencv-python.git/
	else
	    (cd opencv-python && git pull)
	fi
	pip install numpy scikit-build cmake
	cd opencv-python
	# python -m pip debug --verbose により適切なバージョン(11_0など)を調べ、それに合わせ環境変数(11.0など)を設定する。
	MACOSX_DEPLOYMENT_TARGET=11.0 CC=clang CXX=clang++ python3 setup.py bdist_wheel
	pip install dist/opencv_python-*.whl
	;;
    PyQt5 | pyqt5 | PyQt6 | pyqt6)
	pip install PyQt6
	;;
    bazel)
	install_bazel
	;;
    tensorflow_addons | tensorflow-addons)
	install_bazel
	install_grpcio
	pip install tensorflow
	export HDF5_DISABLE_VERSION_CHECK=1
	if [ ! -d addons ]; then
	    #git clone -b v0.13.0 https://github.com/tensorflow/addons.git/
	    #git clone -b mywork https://github.com/tetsuyasu/addons.git/
	    git clone https://github.com/tensorflow/addons.git/
	fi
	cd addons

	# This script links project with TensorFlow dependency

	python3 ./configure.py

	bazel build build_pip_pkg

	pip install pathlib
	#patch -f -p1 < $DIR/addons.patch || echo OK
	bazel-bin/build_pip_pkg artifacts

	pip install artifacts/tensorflow_addons-*.whl
	;;
    torchvision)
	if [ ! -d vision ]; then
	    git clone https://github.com/pytorch/vision.git/
	fi
	cd vision
	MACOSX_DEPLOYMENT_TARGET=11.0 CC=clang CXX=clang++ python3 setup.py install
	;;
    llvmlite)
	install_llvmlite
	;;
    tslearn)
	install_llvmlite
	pip install "$1"
	;;
    *)
	install_grpcio
	pip install "$1"
	;;
esac

echo "OK $0"

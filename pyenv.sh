#!/bin/bash

set -e

function replace() {
    _from="$1"
    _to="$2"
    grep -q "^$_from" .envrc || _not_found=1
    if [ "$_not_found" == "1" ]; then
	echo "$_to" >> .envrc
    else
	sed -i -e "s|^$_from.*$|$_to|" .envrc
	rm -f .envrc-*
    fi
}

DIR="$(cd "$(dirname "$0")"; pwd)"

# envs
#PYTHON="${PYTHON:-3.9.7}"
PYTHON="${PYTHON:-3.7.11}"
REPO="$(basename "$PWD")"

# direnv
"$DIR/direnv.sh"

# install pyenv
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if [ -x "$HOME/.pyenv/bin/pyenv" ]; then
    echo found pyenv, good.
else
    if type brew >/dev/null 2>&1; then
	brew install pyenv
	brew install pyenv-virtualenv
    else
	git clone git://github.com/yyuu/pyenv.git ~/.pyenv
	git clone https://github.com/pyenv/pyenv-virtualenv.git "$(pyenv root)/plugins/pyenv-virtualenv"
    fi
    hash -r
fi
which pyenv
pyenv --version

# install python & make virtualenv
if type brew >/dev/null 2>&1; then
    #pyenv install --patch $PYTHON < <(curl -sSL https://github.com/python/cpython/commit/8ea6353.patch\?full_index\=1)
    pyenv install -s -v --patch "$PYTHON" < "$DIR/8ea6353.patch"
else
    pyenv install -s -v "$PYTHON"
fi
pyenv global system

# add pyenv
replace "export PYENV_ROOT=" "export PYENV_ROOT=\$HOME/.pyenv"
case "$PYTHON" in
    3*)
	replace "export PATH=\"\$PYENV_ROOT" "export PATH=\"\$PYENV_ROOT/versions/$PYTHON/bin:\$PYENV_ROOT/bin:\$PATH"\"
	replace 'layout' "layout pyenv $PYTHON"
	;;
    2*)
	pyenv virtualenv -f "$PYTHON" "$REPO"
	replace "export PATH=\"\$PYENV_ROOT/versions/" "export PATH=\"\$PYENV_ROOT/versions/$PYTHON/envs/$REPO-$PYTHON/bin:\$PYENV_ROOT/bin:\$PATH\""
	replace 'layout' "layout activate $REPO"
	;;
esac
replace 'unset PS1' "unset PS1"

# enable direnv
direnv allow
#direnv exec . python3 -V

echo "OK $0"

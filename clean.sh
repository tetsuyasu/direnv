#!/bin/bash

set -e

rm -rf .direnv .envrc .venv .python-version .local venv

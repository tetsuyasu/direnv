#!/bin/bash

function replace() {
    _from="$1"
    _to="$2"
    grep -q "^$_from" .envrc || _not_found=1
    if [ "$_not_found" == 1 ]; then
	echo .envrc not found, create it.
	echo "$_to" >> .envrc
    else
	sed -i -e "s|^$_from.*$|$_to|" .envrc
	rm -f .envrc-*
    fi
}

DIR="$(cd "$(dirname "$0")"; pwd)"

if [ "$(arch)" = "arm64" ] || [ "$(arch)" = "i386" ]; then # Mac
    for i in $(brew list | grep python@); do
	if [ "$i" = "$PYTHON" ]; then
	    brew link $i
	fi
    done
elif type apt-get >/dev/null 2>&1; then # Linux
    dpkg -V python3-venv >/dev/null 2>&1
    if [ "$?" == 0 ] ; then
	echo python3-venv found, good.
    else
	if type sudo >/dev/null 2>&1; then
	    sudo apt-get install -y python3-venv
	else
	    apt-get update
	    apt-get install -y python3-venv
	fi
    fi
fi

# direnv
"$DIR/direnv.sh"

# direnv config
mkdir -p ~/.config/direnv
cp "$DIR/direnvrc" ~/.config/direnv/

# add venv
if [ "$PYTHON" != "" ]; then
    replace layout "layout python /opt/homebrew/bin/$(echo $PYTHON|sed -e 's/@//g')"
elif [ -x /usr/bin/python3 ]; then
    replace layout 'layout python /usr/bin/python3'
else
    replace layout 'layout python3'
fi

# enable direnv
direnv allow

echo "OK $0"

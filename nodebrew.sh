#!/bin/bash

set -e

# install nodebrew
if type nodebrew >/dev/null 2>&1; then
    echo "nodebrew found, OK"
    exit 0
elif type brew >/dev/null 2>&1; then
    brew install nodebrew
else
    curl -L git.io/nodebrew | perl - setup
fi
cat <<EOF
Please add
export PATH="\$HOME/.nodebrew/current/bin:\$PATH"' in your .bashrc/.zshrc"
in your .bashrc/.zshrc.
EOF

# settings
export PATH="$HOME/.nodebrew/current/bin:$PATH"
hash -r
nodebrew setup

echo "OK $0"

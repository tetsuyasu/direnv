#!/bin/bash

set -e

# env check
if [ "$PYENV_VIRTUAL_ENV" != "" ]; then
    echo $PYENV_VIRTUAL_ENV
elif [ "$VIRTUAL_ENV" != "" ]; then
    echo $VIRTUAL_ENV
else
    echo please activate python3 environment.
    exit 1
fi

if [ $# == 1 ]; then
    NODE=$1
else
    NODE=latest
fi

# install node
if [ "$(nodebrew list | grep "^${NODE}$")" == "" ]; then
    if [ "$(arch)" == arm64 ]; then
	rosetta2 nodebrew install-binary $NODE
    else
	nodebrew install-binary $NODE
    fi
fi

# setup direnv
if [ "$(grep nodebrew .envrc)" != "" ]; then
    sed -i -e "s/nodebrew.*/nodebrew use $NODE/" .envrc
else
    echo "nodebrew use $NODE" >> .envrc
fi
direnv allow
direnv reload

echo "OK $0"

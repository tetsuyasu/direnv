#!/bin/bash

set -e

function replace() {
    _from="$1"
    _to="$2"
    grep -q "^$_from" .envrc || _not_found=1
    if [ "$_not_found" == "1" ]; then
	echo "$_to" >> .envrc
    else
	sed -i -e "s/^$_from.*$/$_to/" .envrc
	rm -f .envrc-*
   fi
}

DIR="$(cd "$(dirname "$0")"; pwd)"

# envs
NODE="${NODE:-'stable'}"

# direnv
"$DIR"/direnv.sh

# clean
#rm -rf ~/.nodebrew ~/.npm

# install nodebrew
if type nodebrew >/dev/null 2>&1; then
    echo "nodebrew found, OK"
else
    curl -L git.io/nodebrew | perl - setup
    export PATH="$HOME/.nodebrew/current/bin:$PATH"
fi

# node
nodebrew selfupdate
nodebrew install "$NODE" || echo OK
nodebrew use "$NODE"
hash -r
echo "node=$(node -v)"

# npm
#npm update -g npm
echo "npm=$(npm -v)"

# .envrc
replace '/.nodebrew/current/bin' 'export PATH="'"${HOME}"'/.nodebrew/current/bin:$''PATH"'
replace 'nodebrew use' "nodebrew use $NODE"

# enable direnv
direnv allow

echo "OK $0"

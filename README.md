# direnv

## 使いかた

### clone
```
$ mkdir ~/git
$ cd ~/git
$ git clone https://gitlab.com/tetsuyasu/direnv.git/
```

### install
```
$ cat ~/git/direnv/direnvrc >> ~/.bashrc
$ source ~/.bashrc
```

### usage
```
$ mkdir ~/git/work # ディレクトリ名は変えてください
$ cd ~/git/work
$ ../direnv/venv.sh
```

## プログラムの解説
- venv.sh
  - venv + direnv環境を作る

- pyenv.sh
  - pyenv + direnv環境を作る

- node.sh
  - nodebrew + direnv環境を作る

- all.sh
  - setup.shがない場合はスキップする。
  - pyenv.shがあれば実行する。なければvenv環境を作る。
  - node.shがあれば実行する。
  - setup.shを実行する。

- clean.sh
  - リポジトリ内の隠しファイルを消す。

- m1.sh
  - Apple Silicon用のインストーラ。


## info
- direnv – unclutter your .profile
  - https://direnv.net/

- github wiki
  - https://github.com/direnv/direnv/wiki/Python

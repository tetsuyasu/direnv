#!/bin/bash

DIR=$(cd "$(dirname "$0")"; pwd)

# install direnv
if type direnv >/dev/null 2>&1; then
    echo direnv found, good.
else
    if type apt-get >/dev/null 2>&1; then # Linux
	if type sudo >/dev/null 2>&1; then
	    sudo apt-get install -y direnv libssl-dev
	else
	    apt-get update
	    apt-get install -y direnv libssl-dev
	fi
    elif type brew >/dev/null 2>&1; then # Mac
	brew install direnv
    else
	curl -sfL https://direnv.net/install.sh | bash
    fi
fi

# direnv config
mkdir -p ~/.config/direnv
cp "$DIR/direnvrc" ~/.config/direnv/

echo "OK $0"
